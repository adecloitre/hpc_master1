#include <cmath>
#include <iostream>
#include <string>
#include <mpi.h>

#define _Pi 3.1415927

double NB_ITERATION = 10000000;
double PRECISION = 0.000001;

double random_range (double min, double max)
{
    return min + ((max - min) * (rand() / (double)RAND_MAX));
}

double absolu(double a) {
    if(a < 0) a=-a;
    return a;
}

int Carlo_MPI() {
    MPI_Init(NULL, NULL);
    MPI_Comm groupe = MPI_COMM_WORLD;
    int myId;
    int color = 1;
    char subcommunicator;
    double iteration = 0;
    MPI_Comm_rank(groupe,&myId);

    if(myId == 0){
        color = 0;
        subcommunicator = 'A';
    }else{
        subcommunicator = 'B';
    }

    int key = myId;

    MPI_Comm newComm;
    MPI_Comm_split(MPI_COMM_WORLD, color, key, &newComm);

    if(subcommunicator == 'A'){

    }else{
        double r = 1;
        double x = 0;
        double y = 0;
        double i =0;
        bool is_finish = false;
        while(iteration < NB_ITERATION && is_finish == false){
            iteration = iteration + 1;
            x = random_range(-1,1);
            y = random_range(-1,1);
            if(x*x+y*y<r*r){
                i = i + 1;
            }
            double global_i;
            double global_iteration;
            MPI_Reduce(&i, &global_i, 1, MPI_DOUBLE, MPI_SUM, 0,newComm);
            MPI_Reduce(&iteration, &global_iteration, 1, MPI_DOUBLE, MPI_SUM, 0,newComm);
            if(absolu(4*(global_i/global_iteration) - _Pi) < PRECISION){
                is_finish = true;
            }
            MPI_Bcast(&is_finish, 1, MPI_C_BOOL, 0, newComm);
        }
    }
    MPI_Finalize();
    return iteration;
}

int main(int argc, char *argv[]){
    
    std::cout << Carlo_MPI() << std::endl;
    return 1;
}