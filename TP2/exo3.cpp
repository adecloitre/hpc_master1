#include <iostream>
#include <thread>
#include <cmath>
#include <chrono>
#include <vector>
#include <mutex>

// compilation : g++ -std=c++11 -pthread exo1.cpp -o exo1

#define N 10000000

// fonction calculant la somme des entiers entre i0 et i1
void somme(int i0, int i1) {
    std::mutex mon_mutex;
    mon_mutex.lock();
    std::cout << std::this_thread::get_id() << std::endl;
    mon_mutex.unlock();
    double res=0;
    for (int i=i0; i<i1; i++)
        res += i;
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

int main(int argc, char *argv[]){

    std::cout << "Début du main " << std::endl;

    // lancer les threads
    std::vector<std::thread> vecOfThreads;

    std::thread th1(somme, 0, N/2); // appel non bloquant
    std::thread th2(somme, N/2, N); // appel non bloquant
    std::thread th3(somme, N/2, N); // appel non bloquant
    std::thread th4(somme, N/2, N); // appel non bloquant
    std::thread th5(somme, N/2, N); // appel non bloquant
    std::thread th6(somme, N/2, N); // appel non bloquant
    std::thread th7(somme, N/2, N); // appel non bloquant
    std::thread th8(somme, N/2, N); // appel non bloquant
    std::thread th9(somme, N/2, N); // appel non bloquant
    std::thread th10(somme, N/2, N); // appel non bloquant



    vecOfThreads.push_back(std::move(th1));
    vecOfThreads.push_back(std::move(th2));
    vecOfThreads.push_back(std::move(th3));
    vecOfThreads.push_back(std::move(th4));
    vecOfThreads.push_back(std::move(th5));
    vecOfThreads.push_back(std::move(th6));
    vecOfThreads.push_back(std::move(th7));
    vecOfThreads.push_back(std::move(th8));
    vecOfThreads.push_back(std::move(th9));
    vecOfThreads.push_back(std::move(th10));

    std::cout << "Milieu du main " << std::endl;

    for (std::thread & th : vecOfThreads)
    {
        // If thread Object is Joinable then Join that thread.
        if (th.joinable())
            th.join();
    }

    std::cout << "Fin du main" << std::endl;

    return 1;
}