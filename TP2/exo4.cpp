#include <iostream>
#include <thread>
#include <cmath>
#include <chrono>
#include <vector>
#include <mutex>

// compilation : g++ -std=c++11 -pthread exo1.cpp -o exo1

#define N 10000000

// fonction calculant la somme des entiers entre i0 et i1
void somme(int i0, int i1) {
    std::cout << std::this_thread::get_id() << std::endl;

    auto start_t = std::chrono::high_resolution_clock::now();
    double res=0;
    for (int i=i0; i<i1; i++)
        res += i;
    auto end_t = std::chrono::high_resolution_clock::now();

    auto int_t = std::chrono::duration_cast<std::chrono::milliseconds>(end_t - start_t);

    std::cout << "somme :" << int_t.count() << " milliseconds" << std::endl;

    std::cout << res << std::endl;
}

void somme_sqrt(int i0, int i1) {
    std::cout << std::this_thread::get_id() << std::endl;

    auto start_t = std::chrono::high_resolution_clock::now();
    double res=0;
    for (int i=i0; i<i1; i++)
        res += std::sqrt(i);
    auto end_t = std::chrono::high_resolution_clock::now();

    auto int_t = std::chrono::duration_cast<std::chrono::milliseconds>(end_t - start_t);

    std::cout << "somme_sqrt :" << int_t.count() << " milliseconds" << std::endl;

    std::cout << res << std::endl;
}

void somme_sqrt_log(int i0, int i1) {
    std::cout << std::this_thread::get_id() << std::endl;

    auto start_t = std::chrono::high_resolution_clock::now();
    double res=0;
    for (int i=i0; i<i1; i++)
        res += std::sqrt(i) * std::log(i);

    auto end_t = std::chrono::high_resolution_clock::now();

    auto int_t = std::chrono::duration_cast<std::chrono::milliseconds>(end_t - start_t);

    std::cout << "somme_sqrt_log :" << int_t.count() << " milliseconds" << std::endl;

    std::cout << res << std::endl;
}

int main(int argc, char *argv[]){

    std::cout << "Début du main " << std::endl;

    somme(0,N);
    somme_sqrt(0,N);
    somme_sqrt_log(1,N);

    std::thread thread1(somme, 0, N);
    std::thread thread2(somme_sqrt, 0, N);
    std::thread thread3(somme_sqrt_log, 1, N);

    thread1.join(); // appel bloquant
    thread2.join(); // appel bloquant
    thread3.join(); // appel bloquant


    return 1;
}