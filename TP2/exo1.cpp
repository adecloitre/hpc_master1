#include <iostream>
#include <thread>
#include <cmath>
#include <chrono>
#include <vector>
#include <mutex>

// compilation : g++ -std=c++11 -pthread exo1.cpp -o exo1

#define N 10000000

double g1 ;
double g2 ;
double g3 ;

// fonction calculant la somme des entiers entre i0 et i1
void somme(int i0, int i1) {

    std::cout << std::this_thread::get_id() << std::endl;
    auto start_it = std::chrono::high_resolution_clock::now();
    double res=0;
    for (int i=i0; i<i1; i++)
        res += i;
        g1 += res;
        g2 += res;
        g3 += res;

    auto end_it = std::chrono::high_resolution_clock::now();

    auto int_it = std::chrono::duration_cast<std::chrono::milliseconds>(end_it - start_it);

    std::cout << "somme_thread :" << int_it.count() << " milliseconds" << std::endl;

    std::cout << res << std::endl;
}


int main(int argc, char *argv[]){

    std::cout << "Début du main " << std::endl;

    g1 = 0;
    g2 = 0;
    g3 = 0;

    std::vector<std::thread> vecOfThreads;

    auto start_t = std::chrono::high_resolution_clock::now();

    std::thread th1(somme, 0, N/6);
    std::thread th2(somme, N/6, (2*N)/6);
    std::thread th3(somme, (2*N)/6, (3*N)/6);
    std::thread th4(somme, (3*N)/6, (4*N)/6);
    std::thread th5(somme, (4*N)/6, (5*N)/6);
    std::thread th6(somme, (5*N)/6, N);

    vecOfThreads.push_back(std::move(th1));
    vecOfThreads.push_back(std::move(th2));
    vecOfThreads.push_back(std::move(th3));
    vecOfThreads.push_back(std::move(th4));
    vecOfThreads.push_back(std::move(th5));
    vecOfThreads.push_back(std::move(th6));

    for (std::thread & th : vecOfThreads)
    {
        if (th.joinable())
            th.join();
    }


    auto end_t = std::chrono::high_resolution_clock::now();

    auto int_t = std::chrono::duration_cast<std::chrono::milliseconds>(end_t - start_t);

    std::cout << "somme_main :" << int_t.count() << " milliseconds" << std::endl;

    return 1;
}